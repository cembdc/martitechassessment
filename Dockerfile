FROM node:alpine

WORKDIR /martitechassessment
COPY package.json /martitechassessment

RUN npm install
COPY . /martitechassessment

CMD [ "npm", "start" ]
EXPOSE 4001

# docker build -t martitechassessment .

# docker run -p 4001:4001 martitechassessment