const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const app = require('../../src/loaders/express');

describe('GET /hello', () => {
	it('should return Hello World when express is up', done => {
		request(app)
			.get('/hello')
			.expect(httpStatus.OK)
			.end((err, res) => {
				expect(res).to.not.equal(null);
				expect(res).to.not.equal(undefined);
				expect(res).to.have.a.property('text');
				expect(res.text).to.include('Hello World');
				done(err);
			});
	});
});
