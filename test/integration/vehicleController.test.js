const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const sinon = require('sinon');
const app = require('../../src/loaders/express');
const vehiclePoints = require('../data.json');
const { extVehicleService, vehicleService } = require('../../src/service/service.index');

describe('Vehicle API', () => {
	describe('GET /api/v1/vehicle', () => {
		it('should return vehicle list when there are some vehicles to organize by distance between them', done => {
			sinon.stub(extVehicleService, 'getVehiclePoints').resolves(vehiclePoints); // mocked extVehicleService.getVehiclePoints service

			request(app)
				.get('/api/v1/vehicle')
				.expect(httpStatus.OK)
				.end((err, res) => {
					expect(res.body).to.have.a.property('success');
					expect(res.body).to.have.a.property('message');
					expect(res.body).to.have.a.property('data');
					const { message } = res.body;
					const { success } = res.body;
					const { data } = res.body;
					expect(success).to.be.equal(true);
					expect(message).to.include('Succesfull');
					expect(data).to.not.equal(null);
					expect(data).to.not.equal(undefined);
					expect(data).to.be.an('array');
					expect(data[0]).to.be.an('object');
					done(err);
					extVehicleService.getVehiclePoints.restore();
				});
		});

		it('should return not found error when there are no vehicles found', done => {
			sinon.stub(vehicleService, 'getOrderedVehicles').resolves({ success: false }); // mocked vehicleService.getOrderedVehicles service

			request(app)
				.get('/api/v1/vehicle')
				.expect(httpStatus.NOT_FOUND)
				.end((err, res) => {
					expect(res.body).to.have.a.property('success');
					expect(res.body).to.have.a.property('error');
					expect(res.body).to.have.a.property('message');
					expect(res.body).to.have.a.property('data');
					const { message } = res.body;
					const { success } = res.body;
					const { data } = res.body;
					expect(success).to.be.equal(false);
					expect(message).to.include('No vehicle found');
					expect(data).to.be.equal('');
					done(err);
					vehicleService.getOrderedVehicles.restore();
				});
		});

		it('should return bad request error when there is an exception', done => {
			sinon.stub(vehicleService, 'getOrderedVehicles').throwsException(''); // mocked vehicleService.getOrderedVehicles service

			request(app)
				.get('/api/v1/vehicle')
				.expect(httpStatus.BAD_REQUEST)
				.end((err, res) => {
					expect(res.body).to.have.a.property('success');
					expect(res.body).to.have.a.property('error');
					expect(res.body).to.have.a.property('data');
					const { error } = res.body;
					const { success } = res.body;
					const { data } = res.body;
					expect(success).to.be.equal(false);
					expect(error).to.include('Something went wrong');
					expect(data).to.be.equal('');
					done(err);
					vehicleService.getOrderedVehicles.restore();
				});
		});
	});
});
