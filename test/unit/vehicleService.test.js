const { expect } = require('chai');
const sinon = require('sinon');
const { extVehicleService, vehicleService } = require('../../src/service/service.index');
const vehiclePoints = require('../data.json');

describe('vehicleService Test', () => {
	it('getNextPoint should return point closest to Poi 1 is Poi 2', () => {
		const withoutPoi1Points = vehiclePoints.filter(p => p.poiName !== 'Poi 1');

		const point = vehicleService.getNextPoint(
			{
				poiName: 'Poi 1',
				latitude: 40.978696156559884,
				longitude: 29.13730406235724
			},
			withoutPoi1Points
		);
		expect(point).to.not.equal(null);
		expect(point).to.not.equal(undefined);
		expect(point).to.have.property('poiName');
		expect(point).to.have.property('latitude');
		expect(point).to.have.property('longitude');
		expect(point.poiName).to.be.equal('Poi 2');
	});

	it('getNextPoint should return undefined when no vehicle list items', () => {
		const point = vehicleService.getNextPoint(
			{
				poiName: 'Poi 1',
				latitude: 40.978696156559884,
				longitude: 29.13730406235724
			},
			[]
		);
		expect(point).to.equal(undefined);
	});

	it('getOrderedVehicles should return vehicle list when vehicle points exist', async () => {
		sinon.stub(extVehicleService, 'getVehiclePoints').resolves(vehiclePoints); // mocked extVehicleService.getVehiclePoints service

		const res = await vehicleService.getOrderedVehicles();

		expect(res).to.not.equal(null);
		expect(res).to.not.equal(undefined);
		expect(res).to.be.an('object');
		expect(res).to.have.a.property('success');
		expect(res).to.have.a.property('data');
		expect(res.success).to.be.equal(true);
		expect(res.data).to.be.an('array');
		expect(res.data[0]).to.be.an('object');

		extVehicleService.getVehiclePoints.restore();
	});

	it('getOrderedVehicles should return 10 vehicle list when vehicle points exist', async () => {
		sinon.stub(extVehicleService, 'getVehiclePoints').resolves(vehiclePoints); // mocked extVehicleService.getVehiclePoints service

		const res = await vehicleService.getOrderedVehicles();

		expect(res).to.not.equal(null);
		expect(res).to.not.equal(undefined);
		expect(res).to.be.an('object');
		expect(res).to.have.a.property('success');
		expect(res).to.have.a.property('data');
		expect(res.success).to.be.equal(true);
		expect(res.data).to.be.an('array');
		expect(res.data[0]).to.be.an('object');
		expect(res.data.length).to.be.equal(10);

		extVehicleService.getVehiclePoints.restore();
	});

	it('getOrderedVehicles should return empty list when no vehicle points exist', async () => {
		sinon.stub(extVehicleService, 'getVehiclePoints').resolves([]); // mocked extVehicleService.getVehiclePoints service

		const res = await vehicleService.getOrderedVehicles();

		expect(res).to.not.equal(null);
		expect(res).to.not.equal(undefined);
		expect(res).to.be.an('object');
		expect(res).to.have.a.property('success');
		expect(res).to.have.a.property('data');
		expect(res.success).to.be.equal(true);
		expect(res.data).to.be.an('array');
		expect(res.data.length).to.be.equal(0);

		extVehicleService.getVehiclePoints.restore();
	});

	it('getOrderedVehicles should return vehicle point itself when just 1 vehicle point exist', async () => {
		sinon.stub(extVehicleService, 'getVehiclePoints').resolves([
			{
				poiName: 'Poi 1',
				latitude: 40.978696156559884,
				longitude: 29.13730406235724
			}
		]); // mocked extVehicleService.getVehiclePoints service

		const res = await vehicleService.getOrderedVehicles();

		expect(res).to.not.equal(null);
		expect(res).to.not.equal(undefined);
		expect(res).to.be.an('object');
		expect(res).to.have.a.property('success');
		expect(res).to.have.a.property('data');
		expect(res.success).to.be.equal(true);
		expect(res.data).to.be.an('array');
		expect(res.data[0]).to.be.an('object');
		expect(res.data.length).to.be.equal(1);
		expect(res.data[0].poiName).to.be.equal('Poi 1');
		expect(res.data[0].latitude).to.be.equal(40.978696156559884);
		expect(res.data[0].longitude).to.be.equal(29.13730406235724);

		extVehicleService.getVehiclePoints.restore();
	});

	it('getOrderedVehicles should return success false when external vehicle points service is down', async () => {
		sinon.stub(extVehicleService, 'getVehiclePoints').throwsException('Server is down..');
		const res = await vehicleService.getOrderedVehicles();

		expect(res).to.not.equal(null);
		expect(res).to.not.equal(undefined);
		expect(res).to.be.an('object');
		expect(res).to.have.a.property('success');
		expect(res).to.have.a.property('error');
		expect(res.success).to.be.equal(false);
		expect(res.error).to.be.equal('Something went wrong');
	});
});
