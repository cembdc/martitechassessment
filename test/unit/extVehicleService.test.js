const { expect } = require('chai');
const sinon = require('sinon');
const axios = require('axios');
const { extVehicleService } = require('../../src/service/service.index');

describe('extVehicleService Test', () => {
	it("getVehiclePoints should return 'data' of undefined error when null response", async () => {
		const stub = sinon.stub(axios, 'get'); // mocked axios.get service
		stub.withArgs('http://localhost:4000').resolves(null);

		try {
			const res = await extVehicleService.getVehiclePoints();
		} catch (error) {
			expect(error).to.not.equal(null);
			expect(error).to.not.equal(undefined);
			const { message } = error;
			expect(message).to.include("Cannot read property 'data' of undefined");
		}

		axios.get.restore();
	});
});
