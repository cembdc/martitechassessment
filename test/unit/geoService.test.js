const { expect } = require('chai');
const { geoService } = require('../../src/service/service.index');

describe('vehicleService Test', () => {
	it('distance should return 0 when 2 points are the same coordinates', () => {
		const result = geoService.distance(
			40.978696156559884,
			29.13730406235724,
			40.978696156559884,
			29.13730406235724
		);

		expect(result).to.not.equal(null);
		expect(result).to.not.equal(undefined);
		expect(result).to.be.equal(0);
	});

	it('distance should return NaN when one of the coordinates is undefined', () => {
		const result = geoService.distance(-1, 0, 4);

		expect(Number.isNaN(result)).to.be.eq(true);
	});
});
