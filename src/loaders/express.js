const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const bodyparser = require('body-parser');
const routes = require('../routes/routes');

const app = express();

app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json({ limit: '10mb' }));
app.use(helmet());
app.use(cors());

routes.assignRoutes(app);

app.get('/hello', (req, res, next) => {
	res.send('Hello World');
	next();
});

module.exports = app;
