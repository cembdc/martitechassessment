const dotenv = require('dotenv');

dotenv.config();

exports.config = {
	api_config: {
		api_host: process.env.API_HOST,
		api_port: process.env.API_PORT,
		api_base_url: process.env.API_BASE_URL,
		vehicle_api_host: process.env.VEHICLE_API_HOST
	}
};
