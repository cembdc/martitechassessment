const { vehicleRoute } = require('./routes.index');

exports.assignRoutes = app => {
	vehicleRoute.assignRoutes(app);
};
