const { vehicleController } = require('../controller/controller.index');
const { requestUtil } = require('../utils/utils.index');
const { apiResponse } = require('../middlewares/middlewares.index');

exports.assignRoutes = app => {
	/**
	 * Get ordered vehicles by distance between them
	 */
	app.get(requestUtil.getUrlPrefix('vehicle'), vehicleController.getOrderedVehicles, apiResponse.send);
};
