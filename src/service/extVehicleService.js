const axios = require('axios');
const { config } = require('../config/config');

/**
 * @description Gets vehicle points
 *
 * @returns {Promise<{data: [{poiName: string, latitude: number, longitude: number}]}>}
 * {data: [{poiName: string, latitude: number, longitude: number}]}
 */
exports.getVehiclePoints = async () => {
	try {
		const response = await axios.get(config.api_config.vehicle_api_host);
		return response.data;
	} catch (error) {
		console.log(error);
		throw error;
	}
};
