const { extVehicleService, geoService } = require('./service.index');

/**
 *
 * @param {Object} startPoint - The starting point object.
 * @param {string} startPoint.poiName - Point name.
 * @param {number} startPoint.latitude - Latitude of point.
 * @param {number} startPoint.longitude - Longitude of point.
 * @param {Object[]} vehiclePoints - The vehicle point object array.
 * @param {string} vehiclePoints[].poiName - Point name.
 * @param {number} vehiclePoints[].latitude - Latitude of point.
 * @param {number} vehiclePoints[].longitude - Longitude of point.
 *
 * @returns {<{poiName: string, latitude: number, longitude: number}>} vehicle point object
 */
exports.getNextPoint = (startPoint, vehiclePoints) => {
	if (!startPoint || !vehiclePoints) return;
	let minDistance = -1;
	let distance;
	let nextPoint;

	vehiclePoints.forEach(point => {
		distance = geoService.distance(
			startPoint.latitude,
			startPoint.longitude,
			point.latitude,
			point.longitude
		);

		if (minDistance < 0 || distance < minDistance) {
			minDistance = distance;
			nextPoint = point;
		}
	});

	return nextPoint;
};

/**
 * @description Gets ordered vehicles by distance between them
 *
 * @returns {Promise<{success: boolean, error: *} | {success: boolean, data: [*]}>}
 * {success: false, error: any} or {success: true, data: [vehicles]}
 */
exports.getOrderedVehicles = async () => {
	try {
		const orderedList = [];

		let vehiclePoints = await extVehicleService.getVehiclePoints();

		if (!vehiclePoints || vehiclePoints.length <= 0) return { success: true, data: orderedList };

		let startPoint = vehiclePoints[0];

		orderedList.push(startPoint);
		const totalVehicleLength = vehiclePoints.length;

		while (orderedList.length < totalVehicleLength) {
			vehiclePoints = vehiclePoints.filter(p => p.poiName !== startPoint.poiName);

			const nextPoint = this.getNextPoint(startPoint, vehiclePoints);
			if (nextPoint) {
				orderedList.push(nextPoint);
				startPoint = nextPoint;
			}
		}

		return { success: true, data: orderedList };
	} catch (error) {
		console.log(error);
		return { success: false, error: 'Something went wrong' };
	}
};
