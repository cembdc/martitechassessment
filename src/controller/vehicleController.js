const Status = require('http-status');
const { vehicleService } = require('../service/service.index');

exports.getOrderedVehicles = async (req, res, next) => {
	try {
		const result = await vehicleService.getOrderedVehicles();

		if (!result.success) {
			res.apiResponse = {
				status: Status.NOT_FOUND,
				success: result.success,
				message: 'No vehicle found'
			};
		} else {
			res.apiResponse = {
				status: Status.OK,
				success: result.success,
				data: result.data,
				message: 'Succesfull'
			};
		}
	} catch (error) {
		console.log(error);
		res.apiResponse = {
			status: Status.BAD_REQUEST,
			success: false,
			error: 'Something went wrong',
			data: null
		};
	}

	next();
};
