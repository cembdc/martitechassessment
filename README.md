# MartiTechAssessment

[![pipeline status](https://gitlab.com/cembdc/martitechassessment/badges/master/pipeline.svg)](https://gitlab.com/cembdc/martitechassessment/-/commits/master)
[![coverage report](https://gitlab.com/cembdc/martitechassessment/badges/master/coverage.svg)](https://gitlab.com/cembdc/martitechassessment/-/commits/master)

This is the backend project to give an output of some vehicles with coordinates by distance between them. These vehicle informations stores in external reopsitory (https://gitlab.com/martitech-public/backend-assessment).

## Getting Started

Before the installation, we need to run vehicle data project with Docker image(http://registry.gitlab.com/martitech-public/backend-assessment:latest) or cloning repo locally.
When this external project is up and running on http://localhost:4000/ then we can move on to the next step.

### Installation

Clone the repo:

```bash
git clone https://gitlab.com/cembdc/martitechassessment.git
cd martitechassessment
```

Install the dependencies:

```bash
npm install
```

## Commands

### Running locally:

```bash
npm run start
```

### Lint

```bash
# lint code with ESLint
npm run lint
```

### Test

```bash
# run all tests with Mocha
npm run test

# run test coverage
npm run test:coverage
```

### Docker:

```bash
# Build docker image
docker build -t martitechassessment .

# Run docker container
docker run -p 4001:4001 martitechassessment
```

## API Documentation

### API Endpoints

List of available routes:

**Vehicle routes**:\
`GET /api/v1/vehicle` - Get ordered vehicles by distance between them

**Vehicle resposne**:\
The `GET /api/v1/vehicle` response has the following format:

```json
{
	"success": true,
	"error": "",
	"message": "Succesfull",
	"data": [
		{
			"poiName": "Poi 1",
			"latitude": 40.978696156559884,
			"longitude": 29.13730406235724
		},
		{
			"poiName": "Poi 2",
			"latitude": 40.97263724900384,
			"longitude": 29.141359562598126
		},
		{
			"poiName": "Poi 4",
			"latitude": 40.971648978664156,
			"longitude": 29.13672470558623
		},
		{
			"poiName": "Poi 3",
			"latitude": 40.97529416466278,
			"longitude": 29.12781977119488
		},
		{
			"poiName": "Poi 8",
			"latitude": 40.97529416466278,
			"longitude": 29.12781977119488
		},
		{
			"poiName": "Poi 5",
			"latitude": 40.97900394715689,
			"longitude": 29.126296276395397
		},
		{
			"poiName": "Poi 6",
			"latitude": 40.97900394715689,
			"longitude": 29.126296276395397
		},
		{
			"poiName": "Poi 7",
			"latitude": 40.98341005534748,
			"longitude": 29.125395054550385
		},
		{
			"poiName": "Poi 10",
			"latitude": 40.98318327765146,
			"longitude": 29.119558567487996
		},
		{
			"poiName": "Poi 9",
			"latitude": 40.988334180897816,
			"longitude": 29.12507318950866
		}
	]
}
```

or when get error

```json
{
	"success": false,
	"error": "",
	"message": "No vehicle found",
	"data": ""
}
```
